<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sku' => $this->faker->randomNumber(4),
            'category_id' => Category::inRandomOrder()->value('id'),
            'title' => $this->faker->streetName(),
            'description' => $this->faker->text(100),
            'price' => $this->faker->randomFloat(2, 10, 9000),
            'quantity'=>$this->faker->numberBetween(1,100),

        ];
    }
}
