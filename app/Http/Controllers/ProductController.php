<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Cart;

class ProductController extends Controller
{
    public function index(Request $request, int $categoryId = null)
    {
        return view('product.index', [
            'products' =>Product::getByCategory($categoryId),
            'categories' => Category::all(),
        ]);
    }

    public function add(Request $request)
    {
        dd($request->all());
    }
}
