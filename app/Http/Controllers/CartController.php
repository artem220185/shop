<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Http\Requests\CartAddRequest;
use App\Http\Requests\CartUpdateRequest;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $cart = Cart::find(session('cart_id'));

        return view('cart.index', [
            'products' => $cart->products,
            'total_price' => $cart->getTotalPrice(),
        ]);
    }

    public function add(CartAddRequest $request)
    {
        if (empty(session('cart_id'))) {
            $cart = Cart::create([
                'user_id' => Auth::id()
            ]);
            session(['cart_id' => $cart->id]);

        } else {
            $cart = Cart::find(session('cart_id'));
        }
        $cart->addProducts($request->all());
        return redirect()->route('cart');
    }

    public function update(CartUpdateRequest $request)
    {
        $cart = Cart::find(session('cart_id'));
        if (!empty($cart)) {
            $cart->updateProducts($request->except('_token'));

        }
        return redirect()->route('cart');
    }
}
