<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Pagination\LengthAwarePaginator;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    protected $fillable = ['sku', 'category_id', 'title', 'description', 'price', 'quantity'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public static function getByCategory(int $categoryId = null):LengthAwarePaginator
    {
        if (empty($categoryId)) {
            return Product::paginate(9);
        } else {
            return Product::where('category_id', $categoryId)->paginate(9);
        }
    }
}
