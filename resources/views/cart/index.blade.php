@extends('layouts.shop')

@section('content')

    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Shopping Cart </h1>
                <p class="lead text-muted">Cart</p>
                <p>
                    <a href="{{ route('products') }}" class="btn btn-secondary my-2">Products Page</a>
                    <a href="{{ route('cart') }}" class="btn btn-primary my-2">Cart Page</a>
                </p>
            </div>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{ route('cart.update') }}">
                @csrf
                <div class="row">
                    <div class="col-10">
                        <div class="row ">
                            @foreach($products as $product)
                                <div class="col-4">
                                    <div class="card shadow-sm">
                                        <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                                             xmlns="http://www.w3.org/2000/svg" role="img"
                                             aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice"
                                             focusable="false"><title>Placeholder</title>
                                            <rect width="100%" height="100%" fill="#55595c"/>
                                            <text x="50%" y="50%" fill="#eceeef" dy=".3em">{{ $product->title }}</text>
                                        </svg>
                                        <div class="card-body">
                                            <p class="card-text overflow-auto" style="height: 80px">
                                                {{$product->description}}</p>
                                            <p class="card-text"> Price for one:
                                                {{'$'.$product->price}}
                                                <small class="text-muted"> x </small>
                                                {{$product->pivot->quantity}}
                                                <small class="text-muted">thing</small><br>
                                                Total sum:
                                                ${{ $product->price * $product->pivot->quantity}} <br></p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="btn-group">
                                                    <small class="text-muted">Selected Quantity: <input type="number"
                                                                                                        value="{{ $product->pivot->quantity }}"
                                                                                                        name="quantity_{{ $product->id }}"
                                                                                                        min="0"
                                                                                                        max="{{ $product->quantity }}">
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            <h3>Total price: {{ $total_price }} </h3>
        </div>
    </div>

@endsection()
