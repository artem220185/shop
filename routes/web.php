<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('products');
});

Route::middleware(['auth'])->group(function () {
    Route::prefix('products')->group(function () {
        Route::get('/{category_id?}', [ProductController::class, 'index'])->name('products')->whereNumber('category_id');
        Route::post('/add', [ProductController::class, 'add'])->name('products.add');
    });

    Route::prefix('cart')->group(function () {
        Route::get('/', [CartController::class, 'index'])->name('cart');
        Route::post('/add', [CartController::class, 'add'])->name('cart.add');
        Route::post('/update', [CartController::class, 'update'])->name('cart.update');
    });
});

Auth::routes();
Route::get('home', [ProductController::class, 'index'])->name('home');
